FROM python:3.8
MAINTAINER Makhaev Anatoly <greal-san@ya.ru>

ARG PROJNAME=the_hat_game_bot
ARG POETRY_VERSION=1.1.6

ENV PROJNAME=${PROJNAME}
ENV POETRY_VERSION=${POETRY_VERSION}
ENV DEBIAN_FRONTEND noninteractive

RUN mkdir /${PROJNAME}
WORKDIR /${PROJNAME}

# Install linux packages
RUN apt-get update && \
apt-get install --no-install-recommends -y nginx supervisor gettext && \
rm -rf /var/lib/apt/lists/* && \
apt-get purge --auto-remove && \
apt-get clean

# Install gunicorn
RUN pip3 install --no-cache-dir gunicorn

# Install poetry
RUN pip3 install --no-cache-dir "poetry==$POETRY_VERSION"
RUN poetry config virtualenvs.create false

# Setup supervisord
RUN mkdir -p /var/log/supervisor
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
RUN echo "directory=/${PROJNAME}" >> /etc/supervisor/conf.d/supervisord.conf

# Setup nginx
COPY config/flask.conf /etc/nginx/sites-available/flask.conf.template
RUN echo "daemon off;" >> /etc/nginx/nginx.conf

# Install python dependencies
COPY pyproject.toml .
COPY poetry.lock .
RUN poetry install --no-dev --no-interaction --no-ansi

# Setup flask application
COPY application .

# Start processes with setting listen port
CMD /bin/bash -c "envsubst < /etc/nginx/sites-available/flask.conf.template > /etc/nginx/sites-available/default" && /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf