# A bot for playing in The Hat game.
## Setup on your PC
### Installation
First clone this repository:  
```
$ git clone https://gitlab.com/dmia-production-ml-made-team/the-hat-game.git
$ cd the-hat-game
```

Create and activate virtual enviroment:  
```
$ virtualenv -p python3 venv
$ source ./venv/bin/activate
```
For creating enviroment you can use poetry, which is then used to install dependencies:  
```
$ poetry shell  # automatically creates and activates virtual enviroment
```

Install project dependencies:  
```
$ poetry install
```

Run flask-application:  
```
$ cd ./application/
$ python3 main.py -p 8888
```

### Deploy
Remember that the web server integrated in flask is intended for development only. It is not intended to be used in production.
To run the application in production, use a third-party web server, for example, a bundle nginx + gunicorn + flask:  
[How to serve Flask applications with Gunicorn and Nginx on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-18-04-ru)  

### Testing
You can run tests by follow way:  
```
python3 -m pytest tests/
```

## ReplIt
You can test application, without setup on your PC:  
1. Click on link https://replit.com/@gggrafff/TheHatGameBot  
2. Press Fork  
3. Press Run  

## Docker
Container with nginx + gunicorn + flask.  
```
$ docker build -t the_hat_game_bot -f Dockerfile .
$ docker run -it -p 8888:8888 -e PORT=8888 the_hat_game_bot
```

## Heroku
### Deploy via package
```
heroku login
heroku create
heroku buildpacks:set heroku/python
git push heroku master:main
heroku open
```

### deploy via docker
```
heroku container:login
heroku stack:set container
git push heroku master:main
heroku open
```

## How to use it?
### Web-page
The application can be used via the web interface.  
When running according to the instructions above, it will be available at: http://127.0.0.1:8888  

### API
In addition, the application provides a REST API, which can be found at: http://127.0.0.1:8888/api/  

## Additional docs
### virtualenv
[How to use Python virtualenv](https://pythonbasics.org/virtualenv/)  

### Poetry
[Dependency manager Poetry](https://pythonchik.ru/okruzhenie-i-pakety/menedzher-zavisimostey-poetry-polnyy-obzor-ot-ustanovki-do-nastroyki)  

### Flask
[How to make a web application using Flask in Python 3](https://www.digitalocean.com/community/tutorials/how-to-make-a-web-application-using-flask-in-python-3-ru)  
[Flask’s documentation](https://flask.palletsprojects.com/en/1.1.x/)  

### Flask-RESTX
[Flask-RESTX’s documentation](https://flask-restx.readthedocs.io/en/latest/index.html)  

### ReplIt
[Deploying HTTP Servers](https://docs.replit.com/repls/http-servers)  

### Docker
[A complete practical guide to Docker](https://habr.com/ru/post/310460/)  

### Heroku
[Step-by-step manual on heroku for Python](https://devcenter.heroku.com/articles/getting-started-with-python#introduction)  
[Building Docker Images with heroku.yml](https://devcenter.heroku.com/articles/build-docker-images-heroku-yml)  
