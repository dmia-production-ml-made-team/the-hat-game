from flask import Flask
from flask_restx import Api
from config import Configuration
from models.Player import *

app = Flask(__name__)
app.config.from_object(Configuration)
app.url_map.strict_slashes = False

api = Api(app, version='0.1', title='The Hat game bot',
          description='Data mining in action. Spring 2021. The Hat game bot.',
          contact='greal-san@ya.ru',
          license='GPL',
          prefix='/api/',
          doc='/api/'
          )

player = LocalWord2VecPlayer(app.config['MODEL_WORD2VEC_PATH'])
