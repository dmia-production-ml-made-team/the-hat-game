import os


class Configuration(object):
    DEBUG = False
    MODEL_WORD2VEC_PATH = os.path.dirname(os.path.abspath(__file__)) + "/resources/w2v.model"
