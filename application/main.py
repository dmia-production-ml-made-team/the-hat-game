import sys, getopt

from app import app
from views.site import *
from views.api import *


def main(argv):
    port = None

    try:
        opts, args = getopt.getopt(argv, "hp:", ["port="])
    except getopt.GetoptError:
        print('main.py [-p <port>]')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('main.py [-p <port>]')
            sys.exit()
        elif opt in ("-p", "--port"):
            port = arg

    app.run(host='0.0.0.0', port=port)


if __name__ == "__main__":
    main(sys.argv[1:])
