from flask_restx import fields
from app import api

explanation_model = api.model('Explanation', {
    'word': fields.String(required=True, description='The word to explain'),
    'n_words': fields.Integer(min=1, description='Number of hints generated')
})

guess_model = api.model('Guess', {
    'words': fields.List(fields.String, required=True, description='Hint words'),
    'n_words': fields.Integer(min=1, description="Number of assumptions generated")
})

