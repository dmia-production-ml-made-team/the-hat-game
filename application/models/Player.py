import os.path
import gensim


class AbstractPlayer:
    def __init__(self):
        pass

    def explain(self, word, n_words):
        raise NotImplementedError()

    def guess(self, words, n_words):
        raise NotImplementedError()


class LocalStubPlayer(AbstractPlayer):
    def __init__(self):
        super().__init__()

    def explain(self, word, n_words):
        return ['abc'] * n_words

    def guess(self, words, n_words):
        return ['cba'] * n_words


class LocalWord2VecPlayer(AbstractPlayer):
    def __init__(self, model_path):
        super().__init__()
        self.model_path = model_path
        if os.path.isfile(model_path):
            self.model = gensim.models.Word2Vec.load(model_path)

    def fit(self, train_data, epochs=10, **kwargs):
        self.model = gensim.models.Word2Vec(**kwargs)
        self.model.build_vocab(train_data)
        self.model.train(train_data, total_examples=self.model.corpus_count, epochs=epochs)
        self.model.init_sims(replace=True)
        self.model.save(self.model_path)

    def explain(self, word, n_words):
        result = self.model.wv.most_similar(positive=[word], topn=n_words)
        return [x[0] for x in result]

    def guess(self, words, n_words):
        result = self.model.wv.most_similar(positive=words, topn=n_words)
        return [x[0] for x in result]
