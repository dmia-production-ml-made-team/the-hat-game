import pytest


def pytest_configure(config):
    config.addinivalue_line(
        "markers", "post: tests sending POST requests"
    )
    config.addinivalue_line(
        "markers", "get: tests sending GET requests"
    )

