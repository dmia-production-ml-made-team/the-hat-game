import pytest

from app import app
from views.api import *


# python -m pytest tests/


@pytest.fixture(name='client')
def initialize_authorized_test_client():
    app.testing = True
    client = app.test_client()
    yield client
    app.testing = False


@pytest.mark.post
def test_explain_word(client):
    data = {"word": "dollar", "n_words": 7}
    response = client.post('/api/inference/explain', json=data)
    assert response.status_code == 200
    response_json_data = response.get_json()
    assert len(response_json_data) == data["n_words"]


@pytest.mark.post
def test_guess_word(client):
    words = player.explain(word="dollar", n_words=10)
    data = {"words": words, "n_words": 7}
    response = client.post('/api/inference/guess', json=data)
    assert response.status_code == 200
    response_json_data = response.get_json()
    assert len(response_json_data) == data["n_words"]
