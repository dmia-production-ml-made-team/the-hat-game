import pytest
from app import player


# python -m pytest tests/

@pytest.mark.parametrize("word, n_words", [("dollar", 10), ("sport", 5)])
def test_explain_check_words_number(word, n_words):
    prediction = player.explain(word=word, n_words=n_words)
    assert len(prediction) == n_words


@pytest.mark.parametrize("word, n_words", [("dollar", 10), ("sport", 5)])
def test_guess_check_words_number(word, n_words):
    words = player.explain(word=word, n_words=10)
    prediction = player.guess(words=words, n_words=n_words)
    assert len(prediction) == n_words


@pytest.mark.parametrize("word", ["dollar", "sport"])
def test_explain_check_equal_words(word):
    prediction = player.explain(word=word, n_words=10)
    assert word not in prediction


@pytest.mark.parametrize("word", ["dollar", "sport"])
def test_guess_check_equal_words(word):
    words = player.explain(word=word, n_words=10)
    prediction = player.guess(words=words, n_words=10)[0]
    assert prediction not in words


@pytest.mark.parametrize("word", ["dollar", "sport"])
def test_explain_and_guess_word(word):
    words = player.explain(word=word, n_words=10)
    result = player.guess(words=words, n_words=1)[0]
    assert result == word

