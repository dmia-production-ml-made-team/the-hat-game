from flask_restx import Resource
import json
from app import api, player
from models.ApiModels import *


ns = api.namespace('inference', description='Use the model')


@ns.route('/explain')
class Explain(Resource):
    """Explains a word using other words"""
    @ns.doc('Make a explanation')
    @ns.expect(explanation_model)
    @ns.response(fields.List(fields.String), required=True, description='Predicted words')
    def post(self):
        """Explains a word using other words"""
        try:
            words = player.explain(word=api.payload['word'], n_words=api.payload['n_words'])
        except:
            words = []
        return words


@ns.route('/guess')
class Guess(Resource):
    """Guesses which word was explained"""
    @ns.doc('Make a guess')
    @ns.expect(guess_model)
    @ns.response(fields.List(fields.String), required=True, description='Predicted words')
    def post(self):
        """Guesses which word was explained"""
        try:
            words = player.guess(words=api.payload['words'], n_words=api.payload['n_words'])
        except:
            words = []
        return words
