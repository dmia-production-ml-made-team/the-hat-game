from flask import request, render_template, send_from_directory
from app import app, player
import os


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route("/", methods=["POST", "GET"])
def index():
    """Main form rendering"""
    if request.method == 'POST':
        if 'word' in request.form:
            words = player.explain(word=request.form['word'], n_words=int(request.form['n_words']))
            return render_template("index.html", explanation=' '.join(words))
        if 'words' in request.form:
            words = player.guess(words=request.form['words'].split(), n_words=int(request.form['n_words']))
            return render_template("index.html", guess=' '.join(words))
        return render_template("index.html")
    else:
        return render_template("index.html")

